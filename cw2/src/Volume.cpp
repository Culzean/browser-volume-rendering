#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "Volume.h"


Volume::Volume(std::string filename) {
	
    std::ifstream f(filename.c_str());
    if (!f.is_open()) {
      printf("Failed to read file %s\n", filename.c_str()); fflush(stdout);
      exit(EXIT_FAILURE);
    }
    
    f >> m_width; f >> m_height; f >> m_depth;
    
    printf("Volume Width: %i Height: %i Depth: %i\n", m_width, m_height, m_depth);
	printf("Load complete");
    m_data = new unsigned char[m_width * m_height * m_depth];
    
    printf("Load complete");
    
    int val = 0;
    
    for(int z = 0; z < m_depth; z++)
    for(int x = 0; x < m_width; x++)
    for(int y = 0; y < m_height; y++) {
      f >> val;
      Set(x, y, z, val);
    }
    
    f.close();
	
	printf("Load complete");
	//setup my control points, cause manually doing this is a good way to spend my time
	
	alphaKnots = std::vector<TransferControlPoint>();
	alphaKnots.push_back(TransferControlPoint(.05f, 0));
	alphaKnots.push_back(TransferControlPoint(.7f, 40));
	alphaKnots.push_back(TransferControlPoint(0.7f, 60));
	alphaKnots.push_back(TransferControlPoint(0.05f, 63));
	alphaKnots.push_back(TransferControlPoint(0.1f, 80));
	alphaKnots.push_back(TransferControlPoint(0.3f, 82));
	alphaKnots.push_back(TransferControlPoint(1.0f, 256));

	colorKnots = std::vector<TransferControlPoint>();
	colorKnots.push_back(TransferControlPoint(.91f, .7f, .61f, 0));
	colorKnots.push_back(TransferControlPoint(.55f, .7f, .0f, 10));
	colorKnots.push_back(TransferControlPoint(1.0f, 0.7f, .09f, 60));
	colorKnots.push_back(TransferControlPoint(.91f, .7f, .61f, 80));
	colorKnots.push_back(TransferControlPoint(1.0f, 1.0f, .85f, 82));
	colorKnots.push_back(TransferControlPoint(1.0f, 0.0f, .35f, 150));
	colorKnots.push_back(TransferControlPoint(1.0f, 0.4f, .45f, 244));
	colorKnots.push_back(TransferControlPoint(1.0f, 0.0f, 1.0f, 256));
        

    //also set up grandients
    generateGradients(2);
    filterNxNxN(2);
}

Volume::~Volume() {
    delete[] m_data;
}

int Volume::GetDepth() {
    return m_depth;
}

int Volume::GetWidth() {
    return m_width;
}

int Volume::GetHeight() {
    return m_height;
}

Vector3 Volume::GetNormal( int x, int y, int z ) {
    int index = x + y * m_width + z * m_width * m_height;
    return m_gradients[index];
}

void Volume::Set(int x, int y, int z, unsigned char val) {
    int index = x + y * m_width + z * m_width * m_height;
    m_data[index] = val;
}

//Find grandients using the central difference equation, so difference between the next and previous volume.
//quite a crude way to calculate this, but should get some decent results.
void Volume::generateGradients(int sampleSize)
{
    int n = sampleSize;
    Vector3 normal = Vector3::Zero();
    Vector3 s1, s2;
    
    int index = 0;
    for (int z = 0; z < m_depth; z++)
    {
        for (int y = 0; y < m_height; y++)
        {
            for (int x = 0; x < m_width; x++)
            {
                s1.x = Get(x - n, y, z);
                s2.x = Get(x + n, y, z);
                s1.y = Get(x, y - n, z);
                s2.y = Get(x, y + n, z);
                s1.z = Get(x, y, z - n);
                s2.z = Get(x, y, z + n);
                
                m_gradients.push_back( Vector3::Normalize(s2 - s1) );
                if (isnan(m_gradients[index].x)==1)
                    m_gradients[index] = Vector3::Zero();
                index++;
            }
        }
    }
}

//sample nearest neighbours and smooth out any sharp changes.
void Volume::filterNxNxN(int n)
{
    int index = 0;
    for (int z = 0; z < m_depth; z++)
    {
        for (int y = 0; y < m_height; y++)
        {
            for (int x = 0; x < m_width; x++)
            {
                m_gradients[index++] = sampleNxNxN(x, y, z, n);
                //Vector3::Print( m_gradients[index-1]);

            }
        }
    }
}

Vector3 Volume::sampleNxNxN(int x, int y, int z, int n)
{
    n = (n - 1) / 2;
    
    Vector3 average = Vector3::Zero();
    int num = 0;
    
    for (int k = z - n; k <= z + n; k++)
    {
        for (int j = y - n; j <= y + n; j++)
        {
            for (int i = x - n; i <= x + n; i++)
            {
                if (i > 0 && i <= GetWidth() && j > 0 && j <= GetHeight() && k > 0 && k <= GetDepth())
                {
                    average = average + Get(i, j, k);
                    num++;
                }
            }
        }
    }
    
    average = average / (float)num;
    if (average.x != 0.0f && average.y != 0.0f && average.z != 0.0f)
        average = Vector3::Normalize(average);
    
    if (isnan(average.x)==1)
        average = Vector3::Zero();
    return average;
}


//CPU side implementation of BlinnPhongShading
Vector3 Volume::BlinnPhongShading(Light* light, Vector3 _hitPoint, Vector3 _normal, Vector3 _eyePosition, Vector3 _diffuse) {
    
    Vector3 surfaceNormal;
    Vector3 vecToLight;

    Vector3 lightDir = Vector3::Normalize(light->Position() - _hitPoint);
    
    float lambertian = fmax(Vector3::Dot(lightDir, _normal), 0.0f);
    float specular = 0.0f;
    
    if (lambertian > 0.0f) {
        
        Vector3 viewDir = Vector3::Normalize(_eyePosition - _hitPoint);
        
        Vector3 halfVec = Vector3::Normalize(lightDir + viewDir);
        float specAngle = fmax(Vector3::Dot(halfVec, _normal), 0.0f);
        specular = pow(specAngle, 2.33f);
        
    }
    Vector3 colorLinear = ambient * light->Colour() * light->Intensity() +
    (_diffuse * lambertian * light->Colour() * light->Intensity())+
    light->Colour() * (specular * specular) * light->Intensity();
    
    //cout << "Colour in Blinn " << glm::to_string(colorLinear) << endl;
    return colorLinear;
}

TransferControlPoint Volume::FindAlpha(int _iso, std::vector<TransferControlPoint> *vec) {
    
    TransferControlPoint first = vec->at(0);
    TransferControlPoint second = vec->at(0);
    for(int i=0; i<vec->size(); i++) {
        TransferControlPoint crt = vec->at(i);
        if(_iso >= crt.isoValue) {
            first = crt;
            if((i+1)< vec->size()) {
                second = vec->at(i+1);
            }
            else {
                second = first;
            }
            
        }
    }
    
    //interpolate between two points
    int diff = second.isoValue - first.isoValue;
    if(diff <= 0) {
        return first;
    }
    int value = _iso - first.isoValue;
    float t = value / diff;
    Vector4 blendCol = (first.color * t) + first.color;
    TransferControlPoint ret = TransferControlPoint( blendCol.x, blendCol.y, blendCol.z, blendCol.w ,_iso );
    return ret;
}

unsigned char Volume::Get(int x, int y, int z) {
    int index = x + y * m_width + z * m_width * m_height;
    if(index <0 || index >= 6553600) {return 0;}
    return m_data[index];
}
