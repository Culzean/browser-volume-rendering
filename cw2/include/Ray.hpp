//
//  Ray.hpp
//  cw2
//
//  Created by Daniel Waine on 19/03/2016.
//  Copyright © 2016 UOE Daniel Waine. All rights reserved.
//

#ifndef Ray_hpp
#define Ray_hpp

#include <stdio.h>

#include "Vector.h"
#include "Matrix.h"

class Ray {
public:
    Vector3 origin;
    Vector3 direction;
    
    Ray( Vector3 &origin,  Vector3 &_direction):
    origin(origin),
    direction(_direction)
    {}
    
    /* Returns the position of the ray at time t */
    Vector3 operator() ( float &t)  {
        return origin + (direction*t);
    }
};


#endif /* Ray_hpp */
