#ifndef VOLUME_H
#define VOLUME_H

#pragma once

#include <string>
#include <vector>
#include <math.h>
#include <algorithm>
#include "Vector.h"
#include "Light.h"

struct TransferControlPoint {
    
public:
    Vector4 color;
    int isoValue;
    

    TransferControlPoint(float r, float g, float b, float a, int _isoVal) {
        
        color = Vector4(r, b, g, a);
        isoValue = _isoVal;
    }
    
    TransferControlPoint(float r, float g, float b, int _isoVal) {
        
        color = Vector4(r, b, g, 0.f);
        isoValue = _isoVal;
    }
    
    TransferControlPoint(float _alpha, int _isoVal ) {
        
        color = Vector4(0.f, 0.f, 0.f, _alpha);
        isoValue = _isoVal;
    }
};

class Volume {

    public:
    Volume(std::string filename);
    ~Volume();

    int GetWidth();
    int GetHeight();
    int GetDepth();
    Vector3 GetCentre()  {return Vector3(0.f,0.f,0.f);}
    Vector3 GetNormal( int x, int y, int z );
    
    void Set(int x, int y, int z, unsigned char amount);
    unsigned char Get(int x, int y, int z);

    Vector3 BlinnPhongShading(Light* light, Vector3 _hitPoint, Vector3 _normal, Vector3 _eyePosition, Vector3 _diffuse);
    TransferControlPoint FindAlpha(int _iso, std::vector<TransferControlPoint> *vec);
    
    std::vector<Vector3> m_gradients = std::vector<Vector3>();
    
    //need somewhere to store the control points for the transfer function
    std::vector<TransferControlPoint> colorKnots;
    /*std::vector<TransferControlPoint> colorKnots = std::vector<TransferControlPoint>{
                                                            TransferControlPoint(.91f, .7f, .61f, 0),
        TransferControlPoint(.55f, .7f, .0f, 10),
        TransferControlPoint(1.0f, 0.7f, .09f, 60),
                                                            TransferControlPoint(.91f, .7f, .61f, 80),
                                                            TransferControlPoint(1.0f, 1.0f, .85f, 82),
        TransferControlPoint(1.0f, 0.0f, .35f, 150),
        TransferControlPoint(1.0f, 0.4f, .45f, 244),
        TransferControlPoint(1.0f, 0.0f, 1.0f, 256)};*/
                                                            
 	std::vector<TransferControlPoint> alphaKnots;	
       //this should work with c++11.. DICE don't have c++11? - c++17 is on the way! 
    /*std::vector<TransferControlPoint> alphaKnots = std::vector<TransferControlPoint>{
                                                            TransferControlPoint(.05f, 0),
                                                            TransferControlPoint(.7f, 40),
                                                            TransferControlPoint(0.7f, 60),
                                                            TransferControlPoint(0.05f, 63),
                                                            TransferControlPoint(0.1f, 80),
                                                            TransferControlPoint(0.3f, 82),
                                                            TransferControlPoint(1.0f, 256)};*/
    
    private:

    Vector3 ambient = Vector3(0.34f,0.28f,0.22f);
    Vector3 diffuse = Vector3(0.22f,0.5f,0.9f);
    Vector3 specular = Vector3(1.f,1.f,1.f);
    
    int m_width;
    int m_height;
    int m_depth;

    unsigned char* m_data;
	
    void generateGradients(int sampleSize);
    void filterNxNxN(int n);
    Vector3 sampleNxNxN(int x, int y, int z, int n);
};


#endif
