#pragma once

#include "Ray.hpp"
#include "Vector.h"

class Material;

class Light {

public :
    Light( Vector3 _position, Vector3 _col,float _intensity ) : position(_position), intensity(_intensity), colour(_col) {}
	Light() {}
	~Light() {};

    Vector3 Position() {return position;}
	Vector3 Colour() { return colour; }
	float Intensity() { return intensity; }

protected:  //  The difference between protected and private is that the protected members will still be available in subclasses.
    Vector3 position;
	Vector3 colour;
	float intensity;

private :


};
