#include <stdio.h>
#include <stdlib.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif
#define GLUT_KEY_ESCAPE 27
#ifndef GLUT_WHEEL_UP
#define GLUT_WHEEL_UP 3
#define GLUT_WHEEL_DOWN 4
#endif

#include "Vector.h"
#include "Matrix.h"
#include "Volume.h"
#include "Ray.hpp"
#include "Light.h"
#include <cmath>

#define WIDTH 768
#define HEIGHT 768

static Volume* head = NULL;
static unsigned char threshold = 255;
//static float threshold = 1.0f;

static Matrix_4x4 rot;
static unsigned char timer = 0.0f;
static int drawType = 0;
static Light *light;

void Update() {
    timer += 1;
    glutPostRedisplay();
}

Vector4 Transfer(int x) {
    TransferControlPoint alphaControl = head->FindAlpha(x, &head->alphaKnots);
    TransferControlPoint colControl = head->FindAlpha(x, &head->colorKnots);
    
    //printf("the alpha %f", alphaControl.color.w);
    
    //return Vector4( colControl.color.x, colControl.color.y, colControl.color.z, alphaControl.color.w );
    float a = (float)x / threshold;
    //float a = (float)alphaControl.color.w * threshold;
    //return Vector4(1,1,1,1) * a + Vector4(0,0,0,1) * (1-a);
    return Vector4(colControl.color.xyz(), 1.0f) * a + Vector4(0,0,0,1) * (1-a);
}

void Draw() {
  
  //printf("Draw");
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_MULTISAMPLE);
    glPointSize(2.0);
    glBegin(GL_POINTS);
    rot = Matrix_4x4::RotationX(M_PI/2) * Matrix_4x4::RotationY(M_PI/88 * timer);
    Vector3 camPos = Vector3( 0.f, 4.f, 30.f );
    Vector3 camDir = Vector3( 0.f, -0.3f, -0.5f );
    camDir = camDir.Normalize(camDir);
    camDir = ( rot * Vector4(camDir, 1.f)).xyz();
    //Vector3 S = head->GetCentre() - (camPos * -camDir);
    Vector3 S = Vector3( 0.f, camPos.y, camPos.z );
    //define plane for screen
    Vector3 U = ( rot * Vector4(1,0,0,1)).xyz();
    Vector3 V = ( rot * Vector4(0,1,0,1)).xyz();
    Vector3 ScreenSpace = S - U * (WIDTH/4.f)  - V * (HEIGHT/4.f);
    
    //printf("Start");
    
    //iterate over each pixel
    for(int i=0; i<WIDTH; i++) {
        for(int j=0; j< HEIGHT; j++) {
            
            Vector3 P = ScreenSpace + (U*i/2) + (V * j/2);

            Ray ray = Ray(P, camDir);
            int k = 0;
            float sample = 1.f;
            float accum = 0.0f;
            Vector3 color = Vector3();
            bool inVol = true;
            
            if(drawType == 0) {
                
                TransferControlPoint max = TransferControlPoint(0.f,2);
                
                while( inVol ) {
                    
                    float step = k++*sample;
                    Vector3 samplePos = ray(step);
                    //Reset origin to corner and find sampled voxel
                    int x = (int)(samplePos.x) + head->GetWidth()/2;
                    int y = (int)samplePos.y + head->GetHeight() / 2;
                    int z = (int)samplePos.z + head->GetHeight()/2;
                    
                    if( x > 0 && x <= head->GetWidth() && y > 0 && y <= head->GetHeight() && z > 0 && z <= head->GetDepth() ) {
                        float val = head->Get(x, y, z);
                        
                        if(max.isoValue < val) {
                            max = TransferControlPoint( 1.0f, 1.0f, 1.0f, val );
                        }
                        
                    }
                    else {
                        inVol = false;
                    }
                    
                    float diff = ( max.isoValue > 0 ) ? max.isoValue : 0.0f;
                    glColor3f(diff/threshold, diff/threshold, diff/threshold);
                    glVertex3f(i, j*2.56, 0);
                    
                }
                
            }
            else {
                //draw composite
                while( inVol && accum <= 0.99f) {
                    
                    float step = k++*sample;
                    Vector3 samplePos = ray(step);
                    //Reset origin to corner and find sampled voxel
                    int x = (int)(samplePos.x) + head->GetWidth()/2;
                    int y = (int)samplePos.y + head->GetHeight() / 2;
                    int z = (int)samplePos.z + head->GetHeight()/2;
                    
                    if( x > 0 && x <= head->GetWidth() && y > 0 && y <= head->GetHeight() && z > 0 && z <= head->GetDepth() ) {
                        float val = head->Get(x, y, z);
                        
                        //accumulation equation
                        Vector4 crtCol = Transfer(val);
                        accum += crtCol.w + (accum * (1-crtCol.w));
                        color += crtCol.xyz() + color * (1 - crtCol.w);
                        
                        if(drawType > 1) {
                            Vector3 norm = head->GetNormal(x,y,z);
                            Vector3 shadedCol = head->BlinnPhongShading(light, samplePos, norm, camPos, color);
                            
                            glColor3f(shadedCol.r(), shadedCol.g(), shadedCol.b());
                            glVertex3f(i, j*2.56, 0);
                        }
                        else {

                            glColor3f(color.r(), color.g(), color.b());
                            glVertex3f(i, j*2.56, 0);
                        }

                        
                    }
                    else {
                        inVol = false;
                    }
                    
                }
            }
            
            
            
        }
        
    }
  
	glEnd();
	
	glFlush();
	glutSwapBuffers();
  
}

void KeyEvent(unsigned char key, int x, int y) {

	switch (key) {
        case GLUT_KEY_ESCAPE:
            exit(EXIT_SUCCESS);
            break;
        case GLUT_KEY_UP:
            threshold = (threshold >= 255) ? 255 : threshold+8;
            break;
        case GLUT_KEY_DOWN:
            threshold = (threshold <= 0) ? 0 : threshold-8;
            break;
        case 32:
            if(++drawType > 2) {
                drawType = 0;
            }
            break;
	}
	
}

void KeyEventSpecial(int key, int x, int y) {
    KeyEvent(key, x, y);
}

int main(int argc, char **argv) {

	light = new Light(Vector3(0.f, -1.f, 0.5f), Vector3(0.4f, 0.29f, 0.5f) , 1.2f );
    head = new Volume("head");
    
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB|GLUT_DOUBLE|GLUT_DEPTH|GLUT_MULTISAMPLE);
    glutInitWindowSize(WIDTH, HEIGHT);
    glutCreateWindow("volume");
    
    glClearColor(0.5, 0.5, 0.5, 1.0);
      
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WIDTH, HEIGHT, 0, -256, 256);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glDisable(GL_DEPTH_TEST);
    
    glutKeyboardFunc(KeyEvent);
    glutSpecialFunc(KeyEventSpecial);
    glutDisplayFunc(Draw);
    glutIdleFunc(Update);
    
    glutMainLoop();
    
    delete light;
    delete head;
    
};
